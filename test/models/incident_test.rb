require 'test_helper'

class IncidentTest < ActiveSupport::TestCase
	setup do
		@incident = Incident.new({
			:name => 'Abcde', :description=>'1234567891',
			:priority=>2, :responsible_id=>1, :incident_category_id=>1
		})
	end

	context 'incident with no attrs assigned' do
		should 'not be valid' do
			empty_incident = Incident.new
			refute empty_incident.valid?
		end
	end

	context 'incident without name' do
		should 'not be valid' do
			@incident.name = nil;
			refute @incident.valid?
		end
	end

	context 'incident with empty name' do
		should 'not be valid' do
			@incident.name = ''
			refute @incident.valid?
		end
	end

	context 'incident with a name with less than 5 characters' do
		should 'not be valid' do
			@incident.name = 'abcd'
			refute @incident.valid?
		end
	end

	context 'incident without description' do
		should 'not be valid' do
			@incident.description = nil;
			refute @incident.valid?
		end
	end

	context 'incident with empty description' do
		should 'not be valid' do
			@incident.description = ''
			refute @incident.valid?
		end
	end

	context 'incident with a description shorter than 10 characters' do
		should 'not be valid' do
			@incident.description = 'a' * 9;
			refute @incident.valid?
		end
	end

	context 'incident without priority' do
		should 'not be valid' do
			@incident.priority = nil;
			refute @incident.valid?
		end
	end

	context 'incident with priority less than 1' do
		should 'not be valid' do
			@incident.priority = 0;
			refute @incident.valid?
		end
	end

	context 'incident with priority higher than 10' do
		should 'not be valid' do
			@incident.priority = 11;
			refute @incident.valid?
		end
	end

	context 'incident with a non-integer priority' do
		should 'not be valid' do
			@incident.priority = 11.34
			refute @incident.valid?
		end
	end

	context 'incident without responsble' do
		should 'not be valid' do
			@incident.responsible = nil;
			refute @incident.valid?
		end
	end

	context 'incident without category' do
		should 'not be valid' do
			@incident.incident_category = nil;
			refute @incident.valid?
		end
	end

	context 'incident with valid attributes' do
		should 'be valid' do
			assert @incident.valid?
		end
	end
end
