require 'test_helper'

class ResponsibleTest < ActiveSupport::TestCase

  context 'responsible without name' do
    should 'not be valid' do
      responsible = Responsible.new
      refute responsible.valid?
    end
  end

  context 'responsible with an empty name' do
    should 'not be valid' do
      responsible = Responsible.new
      responsible.name = ''
      refute responsible.save
    end
  end

  context 'responsible with a name with less than 5 characters' do
    should 'not be valid' do
      responsible = Responsible.new
      responsible.name = 'Ben'
      refute responsible.valid?
    end
  end

  context 'responsible with correct data' do
    should 'be valid' do
      responsible = Responsible.new
      responsible.name = 'Pedro Sanchez'
      assert responsible.valid?
    end
  end
end
