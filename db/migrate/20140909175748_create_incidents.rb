class CreateIncidents < ActiveRecord::Migration
  def change
    create_table :incidents do |t|
      t.string :name
      t.text   :description
      t.integer    :priority
      t.references :responsible, index: true
      t.references :incident_category, index: true

      t.timestamps
    end
  end
end
