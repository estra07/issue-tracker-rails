class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.string :keywords
      t.integer :responsible_id
      t.integer :incident_category_id
      t.integer :priority
  
  

      t.timestamps
    end
  end
end
