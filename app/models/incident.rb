class Incident < ActiveRecord::Base
  belongs_to :responsible, inverse_of: :incidents
  belongs_to :incident_category, inverse_of: :incidents

  validates :priority, presence: true, numericality: {only_integer: true},
					             inclusion: { in: 1..10 }

  validates :description, presence: true, length: { minimum: 10 }

  validates :name, presence: true, uniqueness: true, length: { minimum: 5 }

  validates :responsible, presence: true

  validates :incident_category, presence: true
end
