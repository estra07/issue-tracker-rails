require 'test_helper'

class ResponsiblesControllerTest < ActionController::TestCase
  setup do
    @responsible = responsibles(:juan)
  end

  context 'GET #index' do
    should 'return success' do
      get :index
      assert_response :success
      assert_not_nil assigns(:responsibles)
    end
  end

  context 'GET #new' do
    should 'return success' do
      get :new
      assert_response :success
    end
  end

  context 'POST #create' do
    should 'create it and redirect' do
      assert_difference('Responsible.count') do
        post :create, responsible: { name: "John Doe" }
      end

      assert_redirected_to responsible_path(assigns(:responsible))
    end
  end

  context 'GET #show' do
    should 'return success' do
      get :show, id: @responsible
      assert_response :success
    end
  end

  context 'GET #edit' do
    should 'return success' do
      get :edit, id: @responsible
      assert_response :success
    end
  end

  context 'PATCH #update' do
    should 'update it and redirect' do
      patch :update, id: @responsible, responsible: { name: "Jane Doe" }
      assert_redirected_to responsible_path(assigns(:responsible))
    end
  end

  context 'DELETE #destroy' do
    should 'remove it and redirect' do
      assert_difference('Responsible.count', -1) do
        delete :destroy, id: @responsible
      end

      assert_redirected_to responsibles_path
    end
  end
end
