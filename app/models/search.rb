class Search < ActiveRecord::Base
	def incidents
  		@incidents ||= find_incidents
	end

private

	def find_incidents
	  Incident.where(conditions).all
	end

	def keyword_conditions
	  ["incidents.name LIKE ?", "%#{keywords}%"] unless keywords.blank?
	end

	def priority_conditions
	  ["incidents.priority = ?", priority] unless priority.blank?
	end

	def responsible_conditions
	  ["incidents.responsible_id = ?", responsible_id] unless responsible_id.blank?
	end

	def category_conditions
	  ["incidents.incident_category_id = ?", incident_category_id] unless incident_category_id.blank?
	end


	def conditions
	  [conditions_clauses.join(' AND '), *conditions_options]
	end

	def conditions_clauses
	  conditions_parts.map { |condition| condition.first }
	end

	def conditions_options
	  conditions_parts.map { |condition| condition[1..-1] }.flatten
	end

	def conditions_parts
	  private_methods(false).grep(/_conditions$/).map { |m| send(m) }.compact
	end
end
