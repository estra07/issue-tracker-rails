json.array!(@searches) do |search|
  json.extract! search, :id, :keywords, :responsible_id, :incident_category_id, :priority, :new, :show
  json.url search_url(search, format: :json)
end
