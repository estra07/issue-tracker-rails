class Responsible < ActiveRecord::Base
	has_many :incidents, dependent: :destroy, inverse_of: :responsible
	validates :name,  presence: true, uniqueness: true, length: { minimum: 5 }
end
