# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140911133241) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "incident_categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "incidents", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "priority"
    t.integer  "responsible_id"
    t.integer  "incident_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "incidents", ["incident_category_id"], name: "index_incidents_on_incident_category_id", using: :btree
  add_index "incidents", ["responsible_id"], name: "index_incidents_on_responsible_id", using: :btree

  create_table "responsibles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "searches", force: true do |t|
    t.string   "keywords"
    t.integer  "responsible_id"
    t.integer  "incident_category_id"
    t.integer  "priority"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
