require 'test_helper'

class IncidentCategoriesControllerTest < ActionController::TestCase
  setup do
    @incident_category = incident_categories(:bug)
  end

  context "GET #index" do
    should 'return success' do
      get :index
      assert_response :success
      assert_not_nil assigns(:incident_categories)
    end
  end

  context 'GET #new' do
    should 'return success' do
      get :new
      assert_response :success
    end
  end

  context 'POST #create' do
    should 'create a incident category and redirect to incident category path' do
      assert_difference('IncidentCategory.count') do
        post :create, incident_category: { name: "Problema" }
      end

      assert_redirected_to incident_category_path(assigns(:incident_category))
    end
  end

  context 'GET #show' do
    should 'return success' do
      get :show, id: @incident_category
      assert_response :success
    end
  end

  context "GET #edit" do
    should 'return success' do
      get :edit, id: @incident_category
      assert_response :success
    end
  end

  context 'PATCH #update' do
    should 'update the incident and redirect to incident category path' do
      patch :update, id: @incident_category, incident_category: { name: "Error"}
      assert_redirected_to incident_category_path(assigns(:incident_category))
    end
  end

  context 'DELETE #destroy' do
    should 'remove the incident category and redirect to incident categories path' do
      assert_difference('IncidentCategory.count', -1) do
        delete :destroy, id: @incident_category
      end

      assert_redirected_to incident_categories_path
    end
  end
end
