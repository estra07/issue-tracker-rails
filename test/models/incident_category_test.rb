require 'test_helper'

class IncidentCategoryTest < ActiveSupport::TestCase
  context 'category without name' do
    should 'not be valid' do
      cat = IncidentCategory.new

    	refute cat.valid?
    end
  end

  context 'category with empty name' do
    should 'not be valid' do
      cat = IncidentCategory.new
    	cat.name = ''
    	refute cat.valid?
    end
  end

  context 'category with a name with less than 3 characters' do
    should 'not be valid' do
      cat = IncidentCategory.new
      cat.name = 'Ab'
      refute cat.valid?
    end
  end

  context 'category with correct attributes' do
    should 'be valid' do
      cat = IncidentCategory.new
      cat.name = 'Error'
      assert cat.valid?
    end
  end
end
