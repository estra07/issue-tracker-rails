require 'test_helper'

class SearchesControllerTest < ActionController::TestCase
  setup do
    @search = searches(:one)
  end

  context 'GET #new' do
    should 'return success' do
      get :new
      assert_response :success
    end
  end

  context 'POST #create' do
    should 'create new search' do
      assert_difference('Search.count') do
        post :create, search: { incident_category_id: @search.incident_category_id, keywords: @search.keywords, priority: @search.priority, responsible_id: @search.responsible_id}
      end

      assert_redirected_to search_path(assigns(:search))
    end
  end

  context 'GET #show' do
    should 'return success' do
      get :show, id: @search
      assert_response :success
    end
  end
end
