class IncidentCategory < ActiveRecord::Base
	has_many :incidents, dependent: :destroy, inverse_of: :incident_category
	validates :name, presence: true, uniqueness: true, length: { minimum: 3 }
end
