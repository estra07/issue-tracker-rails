json.array!(@incidents) do |incident|
  json.extract! incident, :id, :name, :responsible_id, :incident_category
  json.url incident_url(incident, format: :json)
end
