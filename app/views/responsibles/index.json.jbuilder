json.array!(@responsibles) do |responsible|
  json.extract! responsible, :id, :name
  json.url responsible_url(responsible, format: :json)
end
