require 'test_helper'

class IncidentsControllerTest < ActionController::TestCase
  setup do
    @incident = incidents(:one)
    @responsible = responsibles(:juan)
    @category = incident_categories(:bug)
  end

  context "GET #index" do
    should 'return success' do
      get :index
      assert_response :success
      assert_not_nil assigns(:incidents)
    end
  end

  context 'GET #new' do
    should 'return success' do
      get :new
      assert_response :success
    end
  end

  context 'GET #new when there\'s no responsibles in the system' do
    should 'show flash message' do
      Responsible.stubs(:exists?).returns(false)
      get :new
      assert_redirected_to incidents_path
      assert_contains flash[:error], "There's no responsibles in the system. Please create one."
    end
  end

  context 'GET #new when there\'s no categories in the system' do
    should 'show flash message' do
      IncidentCategory.stubs(:exists?).returns(false)
      get :new
      assert_redirected_to incidents_path
      assert_contains flash[:error], "There's no category in the system. Please create one."
    end
  end

  context 'POST #create' do
    should 'create the incident, redirect and assign flash message' do
      assert_difference('Incident.count') do
        name = 'Incidente dos'
        description = 'Descripcion del incidente'
        post :create, incident: { incident_category_id: @category.id, name: name, description: description, priority: 3, responsible_id: @responsible.id }
      end
      assert_redirected_to incident_path(assigns(:incident))
      assert_equal 'Issue was successfully created', flash[:notice]
    end
  end

  context 'GET #show' do
    should 'return success' do
      get :show, id: @incident
      assert_response :success
    end
  end

  context 'GET #edit' do
    should 'return success' do
      get :edit, id: @incident
      assert_response :success
    end
  end

  context 'PATCH #update' do
    should 'update the issue and redirect' do
      patch :update, id: @incident, incident: { incident_category: @incident.incident_category, name: @incident.name, responsible_id: @incident.responsible_id }
      assert_redirected_to incident_path(assigns(:incident))
      assert_equal 'Issue was successfully updated', flash[:notice]
    end
  end

  context 'DELETE #destroy' do
    should 'remove the issue and redirect to incidents index' do
      assert_difference('Incident.count', -1) do
        delete :destroy, id: @incident
      end

      assert_redirected_to incidents_path
    end
  end

end
