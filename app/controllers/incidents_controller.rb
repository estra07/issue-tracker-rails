class IncidentsController < ApplicationController
  before_action :set_incident, only: [:show, :edit, :update, :destroy]

  # GET /incidents
  # GET /incidents.json
  def index
    @incidents = Incident.all
  end


  # GET /incidents/1
  # GET /incidents/1.json
  def show
  end

  # GET /incidents/new
  def new
    if !Responsible.exists?
      flash[:error] ||= []
      flash[:error].push("There's no responsibles in the system. Please create one.")
    end
    if !IncidentCategory.exists?
      flash[:error] ||= []
      flash[:error].push("There's no category in the system. Please create one.")
    end
    if flash[:error]
      redirect_to incidents_path
    else
      @incident = Incident.new
    end
  end

  # GET /incidents/1/edit
  def edit
  end

  # POST /incidents
  # POST /incidents.json
  def create
    @incident = Incident.new(incident_params)
    respond_to do |format|
        if @incident.save
          format.html { redirect_to @incident, notice: 'Issue was successfully created' }
          format.json { render :show, status: :created, location: @incident }
       else
          format.html { render :new }
          format.json { render json: @incident.errors, status: :unprocessable_entity }
        end

    end
  end

  # PATCH/PUT /incidents/1
  # PATCH/PUT /incidents/1.json
  def update
    respond_to do |format|
      if @incident.update(incident_params)
        format.html { redirect_to @incident, notice: 'Issue was successfully updated' }
        format.json { render :show, status: :ok, location: @incident }
      else
        format.html { render :edit }
        format.json { render json: @incident.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /incidents/1
  # DELETE /incidents/1.json
  def destroy
    @incident.destroy
    respond_to do |format|
      format.html { redirect_to incidents_url, notice: 'Incident was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_incident
      @incident = Incident.find(params[:id])
    end

    def incident_params
      params.require(:incident).permit(:name, :description, :priority, :responsible_id, :incident_category_id)
    end
end
